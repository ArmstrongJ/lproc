lproc - Utility to List Windows Processes
=========================================

This trivial code is used to enumerate and output all the code currently
being executed by a user.  The code simply uses the Microsoft Windows
API to request the values.  The values are then spat out into a
specified file.

Why?
----

This code can be useful if, for example, you have a 32-bit application
that would like a list of all processes _including 64-bit processes_.
The Windows "EnumProcesses" function will not return 64-bit processes if
called from a 32-bit application.  Instead, you could simply call this
executable with a specified filename and read back the results.

Copyright
---------

This code is available under a GPLv3 license:

    lproc - Utility to List Windows Processes
    Copyright (C) 2014  Jeffrey Armstrong <jeff@rainbow-100.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
