/* lproc - A utility to list Windows processes
 * Copyright (C) 2014 Jeffrey Armstrong <jeff@rainbow-100.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <windows.h>
#include <psapi.h>
#include <shlwapi.h>

#define MODE_LIST   0
#define MODE_QUERY  1
#define MODE_KILL   2

static BOOL IsSameFile(const char *path1, const char *path2);

void usage(const char *progname)
{
    printf("%s\n\n",progname);

    printf("For help:\n\t%s -h\n\n", progname);
    printf("To list processes:\n\t%s <outputfile>\n\n", progname);
    printf("To check if running:\n\t%s -q <fullpath>\n\n", progname);
    printf("To kill:\n\t%s -k <fullpath>\n", progname);
}


int main(int argc, char *argv[])
{
int i;
FILE *fp;
DWORD *aProcesses, cbNeeded, cProcesses;
HANDLE hProc;
HMODULE hMod;
char *textReturn;
int mode;
char *exepath;
char *basename;
char procname[MAX_PATH];

DWORD dwAccess;

int ret;

    if(argc == 1 || argc > 3 || strcmp(argv[1], "-h") == 0) {
        usage(argv[0]);
        return 0;
    }

    mode = MODE_LIST;
    if(argv[1][0] == '-') {
        switch(argv[1][1]) {
            case 'q':
                mode = MODE_QUERY;
                break;
            case 'k':
                mode = MODE_KILL;
                break;
            default:
                usage(argv[0]);
                return 0;
        }
        if(argc < 3) {
            usage(argv[0]);
            return 0;
        }
    }
    
    if(mode == MODE_LIST) {
        fp = fopen(argv[1], "w");
        if(fp == NULL) {
            printf("Could not open output file %s\n", argv[1]);
            return 0;
        }
        exepath = "";
        basename = "";
    } else {
        fp = NULL;
        exepath = argv[2];
        basename = strrchr(exepath, '\\');
        if(basename == NULL) {
            usage(argv[0]);
            printf("ERROR: Must specify a full path with backslashes\n");
            return 0;
        }
        basename++;
    }

    ret = 0;

    aProcesses = (DWORD *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, 1024*sizeof(DWORD));
    if(aProcesses == NULL) {
        cProcesses = 0;
    } else if(EnumProcesses(aProcesses, 1024*sizeof(DWORD), &cbNeeded)) {
        cProcesses = cbNeeded/sizeof(DWORD);
    } else
        cProcesses = 0;

    dwAccess = PROCESS_QUERY_INFORMATION | PROCESS_VM_READ;
    if(mode == MODE_KILL)
        dwAccess = dwAccess | PROCESS_TERMINATE;

    for(i=0; i<cProcesses; i++) {
    
        textReturn = NULL;

        hProc = OpenProcess(dwAccess, FALSE, aProcesses[i]);
        if(hProc != NULL) {
            if(EnumProcessModules(hProc, &hMod, sizeof(hMod), &cbNeeded)) {
                textReturn = (char *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, MAX_PATH*sizeof(TCHAR));
                if(textReturn != NULL) {
                    GetModuleBaseName(hProc, hMod, textReturn, MAX_PATH);
                    
                    /* For query and/or kill operations */
                    if(mode > MODE_LIST && 
                       StrCmpI(textReturn, basename) == 0 &&
                       GetModuleFileNameEx(hProc, NULL, procname, (DWORD)MAX_PATH) > 0) 
                    {
                        if(IsSameFile(exepath, procname)) {
                            switch(mode) {
                                case MODE_KILL:
                                    ret = TerminateProcess(hProc, (UINT)0);
                                case MODE_QUERY:
                                    ret = 1;
                            }
                        }
                    }
                    /* End query and/or kill code  */
                    
                }
            }
            
            CloseHandle(hProc);
        }
        
        if(textReturn != NULL) {
            if(fp != NULL)
                fprintf(fp, "%u,%s\n", aProcesses[i], textReturn);
            HeapFree(GetProcessHeap(), 0, textReturn);
        }
        
    }
    
    HeapFree(GetProcessHeap(), 0, aProcesses);
    
    if(fp != NULL)
        fclose(fp);
    
    return ret;
}

static BOOL IsSameFile(const char *path1, const char *path2)
{
HANDLE h1, h2;
BY_HANDLE_FILE_INFORMATION fi1, fi2;
BOOL ret;

    ret = FALSE;

    if(path1 == NULL || path2 == NULL)
        return FALSE;
        
    if(StrCmpI(path1, path2) == 0)
        return TRUE;
        
    h1 = CreateFile(path1, 0, FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, 
                    NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
                    
    h2 = CreateFile(path2, 0, FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, 
                    NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
                    
    if (h1 != INVALID_HANDLE_VALUE && h2 != INVALID_HANDLE_VALUE) {
        
        if(GetFileInformationByHandle(h1, &fi1) && GetFileInformationByHandle(h2, &fi2))
            ret = fi1.dwVolumeSerialNumber == fi2.dwVolumeSerialNumber &&
                  fi1.nFileIndexHigh == fi2.nFileIndexHigh &&
                  fi1.nFileIndexLow == fi2.nFileIndexLow;
    }
    
    if(h1 != INVALID_HANDLE_VALUE)
        CloseHandle(h1);
    
    if(h2 != INVALID_HANDLE_VALUE)
        CloseHandle(h2);
    
    return ret;
}
