CC = gcc
LINK = gcc
CFLAGS = -Os -DPSAPI_VERSION=1 -mtune=generic
LFLAGS = -Os -lpsapi -mtune=generic

OBJS = lproc.o

TARGET = lproc64.exe

%.o : %.c
	$(CC) $(CFLAGS) -c $< -o $@
	
$(TARGET) : $(OBJS)
	$(LINK) $(OBJS) $(LFLAGS) -o $(TARGET)

clean :
	del *.o
	del $(TARGET)

all : $(TARGET)
